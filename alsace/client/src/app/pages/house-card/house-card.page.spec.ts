import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HouseCardPage } from './house-card.page';

describe('HouseCardPage', () => {
  let component: HouseCardPage;
  let fixture: ComponentFixture<HouseCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HouseCardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HouseCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
