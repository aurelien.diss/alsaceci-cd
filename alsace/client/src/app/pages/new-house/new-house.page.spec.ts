import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewHousePage } from './new-house.page';

describe('NewHousePage', () => {
  let component: NewHousePage;
  let fixture: ComponentFixture<NewHousePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewHousePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewHousePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
